import dash
import dash_auth
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import os

import psycopg2

import pandas as pd

from dash.dependencies import Input, Output

conn = psycopg2.connect('dbname = amazon user = postgres password = AltiuS2010!? host = localhost')
periods = ['daily', 'hourly']

app = dash.Dash(__name__)

app.config.suppress_callback_exceptions = True

app.layout = html.Div(
    style={ 'fontFamily':"Arial"},
    children=[
        html.Div(className="nav-bar", children=[
            html.Div(className="title-section", children=[
                html.Div(className="title", children=[
                    dbc.Col(
                        html.A(
                            html.Img(src="assets/logo.png", height="50px")
                        ),
                    ),
                ]),
                html.H6('lobstr.io', style={'color': 'red', 'font-weight': 'bold'})
            ]),
            html.H6('Amazon Stats', style={'color': '#211758'}),
            html.Div(className="company", children=[
                html.Div(className="title", children=[
                    dbc.Col(
                        html.A(
                            html.Img(src="assets/amazon-logo.png", height="30px")
                        ),
                    ),
                ]),
            ]),
        ]),
    html.Div(id="timeline"),
    html.Div(className="row", style={'text-align':'center'}, children=[
        html.Div(className="four columns", children=[
            html.P('PERIOD', style={'color': '#211758', 'fontWeight': '600'}),
            dcc.Dropdown(
                id='period-value',
                options=[{'label': period, 'value': period} for period in periods],
                value ='daily',
                style={'position':'relative', 'zIndex':'999', 'color': '#211758'}
            )
        ]),
    ]),
    html.Div(id="table_div"),
    dcc.Interval(
        id='interval-component',
        interval=3600 * 1000, #live update, updating from db on hourly basis
        n_intervals=0
    )
])

@app.callback(
    [Output('table_div', 'children')],
    [Input('interval-component', 'n_intervals'), Input('period-value', 'value')]
)

def update_table(n, period):
    table_query = "SELECT t.lower_datetime, t.tempo, " \
                  "t2.total AS total_products, t3.total AS total_product_sellers, t4.total AS total_no_cookies_set, " \
                  "t5.total AS total_attribute_errors, t6.total AS total_seller_page_not_found, t7.total AS total_assertion_error, " \
                  "t8.total AS total_tree_not_initialized " \
                  "FROM ( SELECT DISTINCT lower_datetime, tempo FROM statistics ) AS t " \
                  "LEFT JOIN statistics t2 ON t.lower_datetime = t2.lower_datetime AND t.tempo = t2.tempo AND t2._type = 'total_products' " \
                  "LEFT JOIN statistics t3 ON t.lower_datetime = t3.lower_datetime AND t.tempo = t3.tempo AND t3._type = 'total_product_sellers' " \
                  "LEFT JOIN statistics t4 ON t.lower_datetime = t4.lower_datetime AND t.tempo = t4.tempo AND t4._type = 'total_no_cookies_set' " \
                  "LEFT JOIN statistics t5 ON t.lower_datetime = t5.lower_datetime AND t.tempo = t5.tempo AND t5._type = 'total_attribute_errors' " \
                  "LEFT JOIN statistics t6 ON t.lower_datetime = t6.lower_datetime AND t.tempo = t6.tempo AND t6._type = 'total_seller_page_not_found' " \
                  "LEFT JOIN statistics t7 ON t.lower_datetime = t7.lower_datetime AND t.tempo = t7.tempo AND t7._type = 'total_assertion_error' " \
                  "LEFT JOIN statistics t8 ON t.lower_datetime = t8.lower_datetime AND t.tempo = t8.tempo AND t8._type = 'total_tree_not_initialized'"

    df = pd.read_sql(table_query + " where t.tempo = ('{}');".format(period), conn)
    df = df.sort_values('lower_datetime')
    return [
        dash_table.DataTable(
            id='data-table',
            columns=[{"name": i, "id": i} for i in df[['lower_datetime','total_products','total_product_sellers','total_no_cookies_set', 'total_attribute_errors', 'total_seller_page_not_found', 'total_assertion_error', 'total_tree_not_initialized']]],
            data = df.to_dict('records'),
            style_table={
                'maxHeight': '75vh',
                'overflowY': 'scroll',
                'width': '100%',
                'minWidth': '100%',
            },
            style_cell={
                'fontFamily': 'Open Sans',
                'textAlign': 'center',
                'height': '60px',
                'padding': '2px 22px',
                'whiteSpace': 'inherit',
                'overflow': 'hidden',
                'textOverflow': 'ellipsis',
            },
            style_header={
                'fontWeight': 'bold',
                'backgroundColor': 'white',
            },
            style_data_conditional=[
                {
                    # stripped rows
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                }
            ])
    ]

server = app.server

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run_server(host="0.0.0.0", debug=True, port=port)
